﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSitesi.Models;
using System.IO;

namespace YemekSitesi.Controllers
{
    public class AdminController : Controller
    {


        yemekdbEntities db = new yemekdbEntities();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }
        #region // Slider

        public ActionResult Slider()
        {
            var slider = db.slider.ToList();

            return View(slider);
        }
        public ActionResult SlideEkle()
        {
            return View();
        }
        public ActionResult SlideDuzenle(int SlideID)
        {
            var _slideDuzenle = db.slider.Where(x => x.id == SlideID).FirstOrDefault();
            return View(_slideDuzenle);

        }
        public ActionResult SlideSil(int SlideID)
        {
            try
            {
                db.slider.Remove(db.slider.First(d => d.id == SlideID));
                db.SaveChanges();
                return RedirectToAction("Slider", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        [HttpPost]
        public ActionResult SlideEkle(slider s, HttpPostedFileBase file)
        {
            try
            {
                
                    slider _slide = new slider();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _slide.sliderFoto = memoryStream.ToArray();
                    }
                    _slide.sliderText = s.sliderText;
                    _slide.baslangicTarih = s.baslangicTarih;
                    _slide.bitisTarih = s.bitisTarih;
                    db.slider.Add(_slide);
                    db.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult SlideDuzenle(slider slide, HttpPostedFileBase file)
        {
            try
            {
                
                    var _slideDuzenle = db.slider.Where(x => x.id == slide.id).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _slideDuzenle.sliderFoto = memoryStream.ToArray();
                    }
                    _slideDuzenle.sliderText = slide.sliderText;
                    _slideDuzenle.baslangicTarih = slide.baslangicTarih;
                    _slideDuzenle.bitisTarih = slide.bitisTarih;
                    db.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion
        #region // Yemek
        public ActionResult Yemekler()
        {
            var yemekler = db.yiyecek.ToList();

            return View(yemekler);
        }
        public ActionResult YemekEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult YemekEkle(yiyecek s, HttpPostedFileBase file)
        {
            try
            {

                yiyecek _slide = new yiyecek();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _slide.yiyecekFoto = memoryStream.ToArray();
                }
                _slide.yiyecekText = s.yiyecekText;
                _slide.tarif = s.tarif;
                
                db.yiyecek.Add(_slide);
                db.SaveChanges();
                return RedirectToAction("Yemekler", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        
        public ActionResult YemekDuzenle(int YemekID)
        {
          
                var _yemekDuzenle = db.yiyecek.Where(x => x.id == YemekID).FirstOrDefault();
                return View(_yemekDuzenle);
            
        }

        [HttpPost]
        public ActionResult YemekDuzenle(yiyecek yemek, HttpPostedFileBase file)
        {
            try
            {

                var _yemekDuzenle = db.yiyecek.Where(x => x.id == yemek.id).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _yemekDuzenle.yiyecekFoto = memoryStream.ToArray();
                }
                _yemekDuzenle.yiyecekText = yemek.yiyecekText;
                _yemekDuzenle.tarif = yemek.tarif;
                
                db.SaveChanges();
                return RedirectToAction("Yemekler", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }
        
        public ActionResult YemekSil(int YemekID)
        {
            try
            {
                db.yiyecek.Remove(db.yiyecek.First(d => d.id == YemekID));
                db.SaveChanges();
                return RedirectToAction("Yemekler", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        #endregion
        #region //İçecekler
        public ActionResult icecekler()
        {
            var icecekler = db.icecek.ToList();

            return View(icecekler);
        }
        public ActionResult icecekEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult icecekEkle(icecek s, HttpPostedFileBase file)
        {
            try
            {

                icecek _slide = new icecek();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _slide.icecekFoto = memoryStream.ToArray();
                }
                _slide.icecekText = s.icecekText;
                _slide.tarif = s.tarif;

                db.icecek.Add(_slide);
                db.SaveChanges();
                return RedirectToAction("icecekler", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        public ActionResult icecekDuzenle(int icecekID)
        {

            var _icecekDuzenle = db.icecek.Where(x => x.id == icecekID).FirstOrDefault();
            return View(_icecekDuzenle);

        }

        [HttpPost]
        public ActionResult icecekDuzenle(icecek icecek, HttpPostedFileBase file)
        {
            try
            {

                var _icecekDuzenle = db.icecek.Where(x => x.id == icecek.id).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _icecekDuzenle.icecekFoto = memoryStream.ToArray();
                }
                _icecekDuzenle.icecekText = icecek.icecekText;
                _icecekDuzenle.tarif = icecek.tarif;

                db.SaveChanges();
                return RedirectToAction("icecekler", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        public ActionResult icecekSil(int icecekID)
        {
            try
            {
                db.icecek.Remove(db.icecek.First(d => d.id == icecekID));
                db.SaveChanges();
                return RedirectToAction("icecekler", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        #endregion
        #region //Video
        public ActionResult videolar()
        {
            var videolar = db.video.ToList();

            return View(videolar);
        }
        public ActionResult videoEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult videoEkle(video s, HttpPostedFileBase file)
        {
            try
            {

                video _slide = new video();

                _slide.url = s.url;
                _slide.baslik = s.baslik;
                _slide.eklenmeTarihi = s.eklenmeTarihi;
                db.video.Add(_slide);
                db.SaveChanges();
                return RedirectToAction("videolar", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        public ActionResult videoDuzenle(int videoID)
        {

            var _videoDuzenle = db.video.Where(x => x.id == videoID).FirstOrDefault();
            return View(_videoDuzenle);

        }

        [HttpPost]
        public ActionResult videoDuzenle(video video, HttpPostedFileBase file)
        {
            try
            {

                var _videoDuzenle = db.video.Where(x => x.id == video.id).FirstOrDefault();
                _videoDuzenle.url = video.url;
                _videoDuzenle.baslik = video.baslik;
                _videoDuzenle.eklenmeTarihi = video.eklenmeTarihi;

                db.SaveChanges();
                return RedirectToAction("videolar", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        public ActionResult videoSil(int videoID)
        {
            try
            {
                db.video.Remove(db.video.First(d => d.id == videoID));
                db.SaveChanges();
                return RedirectToAction("videolar", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        #endregion
        #region //Duyuru
        public ActionResult duyurular()
        {
            var duyurular = db.duyuru.ToList();

            return View(duyurular);
        }
        public ActionResult duyuruEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult duyuruEkle(duyuru s, HttpPostedFileBase file)
        {
            try
            {

                duyuru _slide = new duyuru();

                _slide.duyuru1 = s.duyuru1;
                _slide.duyuruBaslik = s.duyuruBaslik;
                _slide.duyuruTarih = s.duyuruTarih;
                db.duyuru.Add(_slide);
                db.SaveChanges();
                return RedirectToAction("duyurular", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        public ActionResult duyuruDuzenle(int duyuruID)
        {

            var _duyuruDuzenle = db.duyuru.Where(x => x.id == duyuruID).FirstOrDefault();
            return View(_duyuruDuzenle);

        }

        [HttpPost]
        public ActionResult duyuruDuzenle(duyuru duyuru, HttpPostedFileBase file)
        {
            try
            {

                var _duyuruDuzenle = db.duyuru.Where(x => x.id == duyuru.id).FirstOrDefault();
                _duyuruDuzenle.duyuru1 = duyuru.duyuru1;
                _duyuruDuzenle.duyuruBaslik = duyuru.duyuruBaslik;
                _duyuruDuzenle.duyuruTarih = duyuru.duyuruTarih;

                db.SaveChanges();
                return RedirectToAction("duyurular", "Admin");

            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        public ActionResult duyuruSil(int duyuruID)
        {
            try
            {
                db.duyuru.Remove(db.duyuru.First(d => d.id == duyuruID));
                db.SaveChanges();
                return RedirectToAction("duyurular", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }
        #endregion
    }
}