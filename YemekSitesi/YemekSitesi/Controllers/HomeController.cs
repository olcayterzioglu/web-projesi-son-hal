﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YemekSitesi.Models;
using System.Globalization;

namespace YemekSitesi.Controllers
{
    public class HomeController : Controller
    {

        yemekdbEntities db = new yemekdbEntities();

        public ActionResult Index()
        {
           
            AnasayfaDTO anasayfa = new AnasayfaDTO();
            anasayfa.slider = db.slider.Where(x => (x.baslangicTarih <= DateTime.Now && x.bitisTarih > DateTime.Now)).ToList();
            anasayfa.yiyecek = db.yiyecek.OrderByDescending(x => x.id).Take(2).ToList();
            anasayfa.icecek = db.icecek.OrderByDescending(x => x.id).Take(2).ToList();
            anasayfa.video = db.video.OrderByDescending(x => x.id).Take(2).ToList();
            return View(anasayfa);
            
        }

        public ActionResult Ye()
        {

            List<yiyecek> yemekler = db.yiyecek.OrderByDescending(x => x.id).ToList();
            return View(yemekler);
        }
        public ActionResult YeDetay(int yiyecekID)
        {

            yiyecek yiyecekDetay = db.yiyecek.FirstOrDefault(x => x.id == yiyecekID );
            return View(yiyecekDetay);
        }

        public ActionResult İç()
        {
            List<icecek> icecekler = db.icecek.OrderByDescending(x => x.id).ToList();
            return View(icecekler);
        }
        public ActionResult İçDetay(int icecekID)
        {

            icecek icecekDetay = db.icecek.FirstOrDefault(x => x.id == icecekID);
            return View(icecekDetay);
        }
        public ActionResult İzle()
        {
            List<video> videolar = db.video.OrderByDescending(x => x.id).ToList();
            return View(videolar);
        }

        public ActionResult Duyurular()
        {
            List<duyuru> duyurular = db.duyuru.OrderByDescending(x => x.id).ToList();
            return View(duyurular);
        }

        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }




    }
    public class AnasayfaDTO
    {
        public List<slider> slider { get; set; }
        public List<duyuru> duyuru { get; set; }
        public List<yiyecek> yiyecek { get; set; }
        public List<icecek> icecek { get; set; }
        public List<video> video { get; set; }
        public List<kullanici> kullanici { get; set; }


    }
}